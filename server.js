const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const shortid = require('shortid');
const mongoose = require('mongoose')

app.use(cors())

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())


app.use(express.static('public'))
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
});

const users =[];
const exercises = [];

const getUsersExercisesById = (id,from,to,limit) => {
  let data = exercises.filter((data) => {
    return data['_id'] == id
  });
  if(from != undefined){
    data = data.filter((data) => {
      let from_date = new Date(from);
      let now_date = new Date(data['date'])
      return from_date <= now_date
    })
    
  }
  if(to != undefined){
    data = data.filter((data) => {
      let to_date = new Date(to);
      let now_date = new Date(data['date'])
      return to_date >= now_date
    })
    
  }
  
  if(data.length > limit){
    data.slice(0,limit-1)
  }
  return data
};



const getUsernamebyId = (id) => {
  for(let i = 0;i<users.length;i++){
    if(users[i]['_id'] == id){
      return users[i]['username'];
    }
  }
}



app.post('/api/exercise/new-user',(req, res) => {
  let user = {
    username: req.body.username,
    _id: shortid.generate()
  };
  users.push(user);
  res.json(user);
});

app.get('/api/exercise/users', (req, res) => {
  res.json(users);
})

app.post('/api/exercise/add', (req, res) => {
  let { userId, description, duration, date } = req.body;

  let dateObj;
  //assuming date is correct
  if(date == ""){
    dateObj = new Date();
  } else {
    dateObj = new Date(date);
  }
  const newExercise = {
    username:getUsernamebyId(userId),
    _id: userId,
    description,
    duration:Number(duration),
    date: dateObj.toDateString()
  };
  exercises.push(newExercise);
  res.json(newExercise);
});

app.get('/api/exercise/log', (req, res) => {
  let { userId,from,to,limit} = req.query;
  
  let exerciseList = getUsersExercisesById(userId, from, to, limit);
  
  res.json({
    _id:userId,
    username: getUsernamebyId(userId),
    count: exerciseList.length,
    log: exerciseList
  })
});




const listener = app.listen(process.env.PORT || 3000, () => {
  console.log('Your app is listening on port ' + listener.address().port)
})
